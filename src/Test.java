import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

public class Test {
    public static void main(String[] args) {
        //Создаем новый двунаправленный список
        LinkedList<String> list = new LinkedList<>();

        //Добавляем новые элементы
        list.add("11");
        list.add("22");
        list.add("33");
        list.add("44");
        list.add("55");
        list.add("22");
        list.addLast("Z"); //Добавим в конец очереди
        list.addFirst("A"); //Добавим в начало очереди
        list.add(1, "B"); //Добавим в позицию с индексом 1

        //Выводим список
        System.out.println("Список: " + list);

        System.out.println("Количество элементов в списке: " + list.size());

        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%s ", list.get(i));
        }
        System.out.println();

        list.remove("22"); //Удалим первый элемент равный "22"
        list.remove(2); //Удалим третий элемент
        System.out.println("Список: " + list);
        list.removeFirst(); //Удалим первый элемент
        list.removeLast(); //Удалим последний элемент
        System.out.println("Список: " + list);
        System.out.println("val = " + list.get(2)); //Выведем третий элемент из списка
        list.set(2, "Hi!"); //Изменим третий элемент
        System.out.println("Список: " + list);

        System.out.println("----------");

        Collections.addAll(list, "QQQ", "WWW", "Hi!");
        Collections.sort(list);
        Iterator i = list.listIterator();
        while (i.hasNext()) {
            System.out.printf("%s ", i.next().toString());
        }
        System.out.println();

        System.out.println("----------");

        System.out.println( "frequency= " + Collections.frequency(list, "Hi!") );
        System.out.println( "binarySearch= " + Collections.binarySearch(list, "55") );
        System.out.println( "min= " + Collections.min(list) );
        System.out.println( "max= " + Collections.max(list) );

        System.out.println("----------");

        LinkedList<String> sub = new LinkedList();
        Collections.addAll(sub, "33", "55");
        System.out.println( "sub= " + Collections.indexOfSubList(list, sub) );

        Collections.replaceAll(list, "Hi!", "Ho");
        System.out.println(list);

        Collections.reverse(list);
        System.out.println(list);

        System.out.println("----------");

        LinkedList list2 = new LinkedList();
        list2.add("str");
        list2.add(10);
        list2.add(new int[]{1, 2, 3});

        i = list2.listIterator();
        int num = 0;
        while (i.hasNext()) {
            Object o = i.next();
            System.out.println( o.getClass() );
            if (o instanceof Integer) {
                System.out.println(num + " - Integer");
            }
            if (o instanceof int[]) {
                System.out.println(num + " - int[]");
            }
            if (o instanceof String) {
                System.out.println(num + " - String");
            }
            num++;
        }

        System.out.println("----------");

        i = list2.descendingIterator();
        while (i.hasNext()) {
            System.out.println(i.next().toString());
        }

        System.out.println("----------");

        list.replaceAll((val) -> "_" + val); //Java8+

        list.forEach((val) -> { //Java8+
            System.out.println(val);
        });
    }
}
